var config = {
    keyFilename: './gcpservice.json',
    projectId: 'meta-gear-253408'
}
const Vision = require('@google-cloud/vision');

const vision= new Vision.ImageAnnotatorClient(config);
module.exports = vision;
