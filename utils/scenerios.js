var options = ["Main Menu","Default","Case Setting","Conv. Freq"]

var result = (data) => {

    var data=data.split('\n')
    for(var i of options){
        if(data.indexOf(i)!=-1){
            return {result:i}
        }
    }
    return {result:"Not Recognized"}

}

module.exports=result

