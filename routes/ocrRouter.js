var express = require('express');
var router = express.Router();
var ocr = require('../utils/ocr.js');
var multer  = require('multer');
var upload = multer({ dest: '../tmp/'});
var fs = require("fs");

/* GET home page. */
router.post('/', upload.single('imagefile'), function(req, res) {
  var file = __dirname + '/images/' + req.file.filename;
  fs.rename(req.file.path, file, function(err) {
    if (err) {
      console.log(err);
      res.send(500);
    } else {
      ocr.textDetection(file).then(data=>{res.send(data[0].fullTextAnnotation.text)})
    }
  });
  
});

module.exports = router;
